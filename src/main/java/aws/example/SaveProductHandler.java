package aws.example;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class SaveProductHandler implements RequestHandler<ProductRequest, ProductResponse> {
    private static final String DB_TITLE = "products";
    private final Regions REGION = Regions.US_EAST_1;
    private DynamoDB dynamoDb;

    @Override
    public ProductResponse handleRequest(ProductRequest productRequest, Context context) {
        this.initDynamoDbClient();
        persistData(productRequest);
        ProductResponse productResponse = new ProductResponse();
        productResponse.setMessage("Item was saved successfully: \n" + productRequest);
        return productResponse;
    }

    private PutItemOutcome persistData(ProductRequest personRequest)
            throws ConditionalCheckFailedException {
        try {
            return this.dynamoDb.getTable(DB_TITLE)
                    .putItem(
                            new PutItemSpec().withItem(new Item()
                                    .withString("id", personRequest.getId())
                                    .withString("name", personRequest.getTitle())
                                    .withString("pictureUrl", personRequest.getPictureUrl())
                                    .withNumber("price", personRequest.getPrice())));
        } catch (Throwable e) {
            throw new UnableToSaveItemException("Unable to add item: ", e);
        }
    }

    private void initDynamoDbClient() {
        AmazonDynamoDBClient client = new AmazonDynamoDBClient();
        client.setRegion(Region.getRegion(REGION));
        this.dynamoDb = new DynamoDB(client);
    }
}
